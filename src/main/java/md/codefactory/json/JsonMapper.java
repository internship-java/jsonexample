package md.codefactory.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.nashorn.api.scripting.JSObject;
import md.codefactory.domain.Comment;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class JsonMapper {
    private static ObjectMapper mapper = new ObjectMapper();

    public static String commentToJson(Comment comment) throws JsonProcessingException {
        String jsonObject = mapper.writeValueAsString(comment);
        return jsonObject;
    }

    public static Comment commentFromJson(String o) throws IOException {
  /*      Comment comment = new Comment();

        comment.setPostId(o.getInt("postId"));
        comment.setId(o.getInt("id"));
        comment.setName(o.getString("name"));
        comment.setEmail(o.getString("email"));
        comment.setBody(o.getString("body"));*/
        Comment comment = mapper.readValue(o, Comment.class);

        return comment;
    }
}
