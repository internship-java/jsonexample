package md.codefactory;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class CommentApp{

    public static void main(String[] args) {
        new SpringApplicationBuilder(CommentApp.class).run(args);
    }
}
