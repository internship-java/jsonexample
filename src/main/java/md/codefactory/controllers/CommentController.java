package md.codefactory.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import md.codefactory.domain.Comment;
import md.codefactory.json.JsonMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping
public class CommentController {

    HttpURLConnection conection;
    private HttpURLConnection postConnection;

    @RequestMapping(value = "/all/",method=RequestMethod.GET)
    public  List <Comment> GETRequest() throws IOException {
        List <Comment> resultComments = new ArrayList<>();

        if (setConnection() == HttpURLConnection.HTTP_OK) {
            StringBuffer response = readURL();
            try {
                JSONArray obj = new JSONArray(response.toString()); // parse the array
                for(int i = 0; i < obj.length(); i++){ // iterate over the array
                    JSONObject o = obj.getJSONObject(i);
                    Comment comment = JsonMapper.commentFromJson(o.toString());
                    resultComments.add(comment);
                }
            } catch (JSONException e){
                e.printStackTrace();
            }

        } else {
            System.out.println("GET NOT WORKED");
        }
        return resultComments;
    }

    private StringBuffer readURL() throws IOException {
        String readLine = null;
        BufferedReader in = new BufferedReader(
                new InputStreamReader(conection.getInputStream()));
        StringBuffer response = new StringBuffer();
        while ((readLine = in.readLine()) != null) {
            response.append(readLine);
        }
        in .close();
        return response;
    }

    private int setConnection() throws IOException {
        URL urlForGetRequest = new URL("http://jsonplaceholder.typicode.com/comments");
        conection = (HttpURLConnection) urlForGetRequest.openConnection();

        conection.setRequestMethod("GET");
        conection.setRequestProperty("userId", "a1bcdef"); // set userId its a sample here

        return  conection.getResponseCode();
    }


    @RequestMapping(value = "/add/" ,method=RequestMethod.POST)
    public void POSTRequest(@RequestBody String jsonBody) throws IOException {
        setPostConnection();
        OutputStream os = postConnection.getOutputStream();
        //byte[] outputBytes = json.getBytes("UTF-8");
        byte[] outputBytes = jsonBody.getBytes("UTF-8");
        os.write(outputBytes);
        os.flush();
        os.close();

        System.out.println("POST Response Code :  " + postConnection.getResponseCode());
        System.out.println("POST Response Message : " + postConnection.getResponseMessage());

        if (postConnection.getResponseCode() == HttpURLConnection.HTTP_CREATED) { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    postConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in .readLine()) != null) {
                response.append(inputLine);
            } in .close();

            // print result
            System.out.println(response.toString());

        } else {
            System.out.println("POST NOT WORKED");
        }
    }

    private void setPostConnection() throws IOException {
        URL obj = new URL("http://jsonplaceholder.typicode.com/comments");

        postConnection = (HttpURLConnection) obj.openConnection();
        postConnection.setRequestMethod("POST");
        postConnection.setRequestProperty("userId", "a1bcdefgh");
        postConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        postConnection.setDoOutput(true);
    }

}
