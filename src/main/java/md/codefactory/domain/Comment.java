package md.codefactory.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

public class Comment {
    private Integer postId;
    private Integer id;
    private String name;
    private String email;
    private String body;
}
